.PHONY: clean

ventoy.img: spline.ks ventoy.json spline-autostart.desktop spline-initial-setup.sh
	./create-ventoy.sh

clean:
	rm -f fedora-live.iso
	rm -f fedora-netinstall.iso
	rm -f ventoy.tar.gz
	rm -rf ventoy/
	rm -f ventoy.img
	rm -f ventoy.img.bmap