#!/usr/bin/env bash

set -e

export VENTOY_RELEASE=1.0.99
export FEDORA_RELEASE_MAJOR=40
export FEDORA_RELEASE_MINOR=1.14

export LOOP_DEVICE=/dev/loop0

for command in "qemu-img" "tar" "axel" "sudo"; do
	if ! command -v "${command}" > /dev/null 2>&1; then
		echo "Error: the \"${command}\" command is needed by this script, but is not installed."
		exit 1
	fi
done

function cleanup() {
    echo "Cleaning up…"
    sudo umount ventoy-data || true
    sudo losetup -d ${LOOP_DEVICE} || true
}

echo "This script will create a disk image, and install Ventoy, a Fedora Netinstall and a Spline Kickstart file automatically. It needs to run some things as root in order to do that."
echo "Press [ENTER] to continue, and [Ctrl + C] to cancel."
read

echo "== Downloading ventoy…"
if [ ! -f ventoy.tar.gz ]; then
    axel "https://github.com/ventoy/Ventoy/releases/download/v${VENTOY_RELEASE}/ventoy-${VENTOY_RELEASE}-linux.tar.gz" -o ventoy.tar.gz
fi

echo "== Extracting ventoy…"
mkdir -p ventoy
tar -xf ventoy.tar.gz -C ventoy

echo "== Creating ventoy image…"
qemu-img create ventoy.img 6G

echo "== Downloading Fedora netinstall…"
if [ ! -f fedora-netinstall.iso ]; then
    axel "https://download.fedoraproject.org/pub/fedora/linux/releases/${FEDORA_RELEASE_MAJOR}/Everything/x86_64/iso/Fedora-Everything-netinst-x86_64-${FEDORA_RELEASE_MAJOR}-${FEDORA_RELEASE_MINOR}.iso" -o fedora-netinstall.iso
fi

echo "== Downloading Fedora live image"
if [ ! -f fedora-live.iso ]; then
    axel "https://download.fedoraproject.org/pub/fedora/linux/releases/${FEDORA_RELEASE_MAJOR}/Workstation/x86_64/iso/Fedora-Workstation-Live-x86_64-${FEDORA_RELEASE_MAJOR}-${FEDORA_RELEASE_MINOR}.iso" -o fedora-live.iso
fi

echo "== Downloading Fedora KDE live image"
if [ ! -f fedora-kde-live.iso ]; then
    axel "https://download.fedoraproject.org/pub/fedora/linux/releases/${FEDORA_RELEASE_MAJOR}/Spins/x86_64/iso/Fedora-KDE-Live-x86_64-${FEDORA_RELEASE_MAJOR}-${FEDORA_RELEASE_MINOR}.iso" -o fedora-live-kde.iso
fi

echo "== Installing ventoy to loop-mounted image…"
# From now on, clean up mounts on failure
trap cleanup EXIT

sudo losetup -P ${LOOP_DEVICE} ventoy.img
echo "y
y
" | sudo ./ventoy/ventoy-*/Ventoy2Disk.sh -i -I ${LOOP_DEVICE}
sudo losetup -d ${LOOP_DEVICE}

echo "== Mounting ventoy data partition to ${PWD}/ventoy-data…"
sudo losetup -P ${LOOP_DEVICE} ventoy.img
mkdir -p ventoy-data
sudo mount ${LOOP_DEVICE}p1 ventoy-data

echo "== Installing fedora image and kickstart file to ventoy-data…"
sudo mkdir -p ventoy-data/ventoy/

sudo cp fedora-netinstall.iso   ventoy-data/Installieren.iso
sudo cp fedora-live.iso         ventoy-data/Ausprobieren.iso
sudo cp fedora-live-kde.iso     ventoy-data/Ausprobieren_KDE.iso
sudo cp spline.ks               ventoy-data/
sudo cp ventoy.json             ventoy-data/ventoy/
sudo cp -r theme                ventoy-data/
