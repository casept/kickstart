# Use graphical install
graphical

# Keyboard layouts
keyboard --vckeymap=de-nodeadkeys --xlayouts='de (nodeadkeys)'

# System language
lang de_DE.UTF-8

# Firewall configuration
firewall --use-system-defaults

# We tried to automate disk selection, but unfortunately the system ends up installed to the USB stick when running on an NVME machine.
# Therefore, device has to be selected manually for now.

# Only consider disks that are likely to be the main disk. All but the first matching one will be ignored.
# * On NVME devices, the usb disk will be called sda, so stop after finding an nvme drive
# * In VMs, the usb disk will also be called sda, so stop after finding the first virtual drive
# * On SATA devices, sda is usually the internal drive, and the usb disk will be called sdb, so stop after finding sda.
#ignoredisk --only-use=nvme0n1

# Partition clearing information
# clear all partitions on the nvme disk or if it does not exist use sda, 
# anaconda stops for confirmation
# note: for full automation add zerombr command to kickstart file
#clearpart --all --drives=nvme0n1 --initlabel

# Disk partitioning information
#autopart --type btrfs

# System timezone
timezone Europe/Berlin --utc

# Lock root account by default (only use sudo)
rootpw --lock

# Set installation source
url --mirrorlist="https://mirrors.fedoraproject.org/mirrorlist?repo=fedora-40&arch=x86_64"
repo --name=fedora --mirrorlist="https://mirrors.fedoraproject.org/mirrorlist?repo=fedora-40&arch=x86_64"
repo --name=fedora-updates --mirrorlist="https://mirrors.fedoraproject.org/mirrorlist?repo=updates-released-f40&arch=x86_64" --cost=0

%packages
@Fedora Workstation
@^workstation-product-environment
vim-enhanced
ghc
nasm
gcc
gcc-c++
clang
autoconf
automake
java-latest-openjdk-devel
git
htop
tmux
okular
pandoc
python3-numpy
python3-scipy
python3-matplotlib
inkscape
krita
foliate
libreoffice
xournalpp
gnome-tweaks
gnome-firmware
gnome-extensions-app
gnome-shell-extension-places-menu
gnome-shell-extension-appindicator
gnome-shell-extension-gsconnect
@development-tools
flatpak
initial-setup
NetworkManager-openconnect
zenity
curl
%end

%pre
nmcli dev wifi c "Installparty" password "Installparty"
%end

%post
  # Add flathub
  flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo

  # Add RPM Fusion
  dnf -y install https://mirrors.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm https://mirrors.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm

  dnf -y groupupdate core
  dnf -y groupupdate multimedia --setop="install_weak_deps=False" --exclude=PackageKit-gstreamer-plugin
  dnf -y groupupdate sound-and-video
  dnf -y install rpmfusion-free-release-tainted
  dnf -y install rpmfusion-nonfree-release-tainted

  # Multimedia
  dnf -y install x264 vlc ffmpeg

  dnf -y install gstreamer1-plugins-{good-\*,base} gstreamer1-plugin-openh264 gstreamer1-libav --exclude=gstreamer1-plugins-bad-free-devel

  # Flatpaks
  flatpak install -y flathub com.github.tchx84.Flatseal com.github.flxzt.rnote org.mozilla.Thunderbird

  # remove abrt
  dnf -y remove gnome-abrt totem

  # Install vscode
  rpm --import https://packages.microsoft.com/keys/microsoft.asc
  sh -c 'echo -e "[code]\nname=Visual Studio Code\nbaseurl=https://packages.microsoft.com/yumrepos/vscode\nenabled=1\ngpgcheck=1\ngpgkey=https://packages.microsoft.com/keys/microsoft.asc" > /etc/yum.repos.d/vscode.repo'
  dnf check-update
  dnf -y install code

  # Install scala3
  mkdir -p /opt/
  curl -L https://github.com/scala/scala3/releases/download/3.5.1/scala3-3.5.1.tar.gz | tar -C /opt/ -xzf -
  export SCALA_ROOT=/opt/scala3-*
  mkdir -p /usr/local/bin/
  ln -s $SCALA_ROOT/bin/scala /usr/local/bin/scala
  ln -s $SCALA_ROOT/bin/scalac /usr/local/bin/scalac
  ln -s $SCALA_ROOT/bin/scaladoc /usr/local/bin/scaladoc

  # Spline initial setup
  mkdir -p /usr/local/bin

  curl -L -o /usr/local/bin/setup-eduroam https://www.zedat.fu-berlin.de/pub/ZEDAT/WLAN/eduroam-linux-eduroam-2019.py
  chmod +x /usr/local/bin/setup-eduroam

  curl -L -o /usr/local/bin/spline-initial-setup https://gitlab.spline.inf.fu-berlin.de/spline/installparty/kickstart/-/raw/initial-setup/spline-initial-setup.sh
  chmod +x /usr/local/bin/spline-initial-setup

  curl -L -o /etc/xdg/autostart/spline-autostart.desktop https://gitlab.spline.inf.fu-berlin.de/spline/installparty/kickstart/-/raw/initial-setup/spline-autostart.desktop
%end
