#!/usr/bin/env bash

# No-op if the setup was already done
if [ -f ~/.spline-setup-ran ]; then
    exit 0
fi

gnome-extensions enable places-menu@gnome-shell-extensions.gcampax.github.com
gnome-extensions disable background-logo@fedorahosted.org
# Seems to not support the GNOME version in Fedora 38 yet
# TODO: Re-enable once it does
# gnome-extensions enable no-overview@fthx
gnome-extensions enable gsconnect@andyholmes.github.io

setup-eduroam


touch ~/.spline-setup-ran
