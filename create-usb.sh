#!/usr/bin/env bash

export USB_DEV="$1"

if [ -z ${USB_DEV} ]; then
	echo "Usage: ./create-usb.sh /dev/<device>"
	exit 1
fi

set -e

bmaptool create -o ventoy.img.bmap ventoy.img
sudo bmaptool copy --bmap ventoy.img.bmap ventoy.img "${USB_DEV}"
